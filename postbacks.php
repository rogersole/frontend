<?php
require "classes/db_management.php";

session_start();
/*** begin the session ***/
if(!isset($_SESSION['publisher_id']))
{
    header('Location:login.php');
}
//**
$publisher_id = $_SESSION['publisher_id'];

$db_class = new db_management();

date_default_timezone_set('UTC');
$hoy = date('Y-m-d'); //01-12-2001
$dia = date('d');
$mes = date('m');
$ano = date('Y');

$date_start = "$ano-$mes-01";
$date_end = "$ano-$mes-30";

if (!empty($_POST)){
    $date_start = $_POST['date_start'];
    $date_end = $_POST['date_end'];
}

$query = "SELECT tag_id from tags where publisher_id = '$publisher_id'";
$conn = $db_class->operation_db_connection();
$tag_query = pg_query($conn, $query);
$tag_fetch = pg_fetch_all($tag_query);
$tag_fetch_json = json_encode($tag_fetch);
$tag_list = str_replace('tag_id', '', str_replace('[', '', str_replace(']', '', str_replace('"', '', str_replace('{', '', str_replace('}', '', str_replace(':', '', $tag_fetch_json)))))));

$query="select date_trunc('day', ts) as date, count(click_id) as postbacks, sum(postback_Amount)/count(click_id) as cpm , sum(postback_amount) as revenue from reporting_postbacks where tag_id in ($tag_list) and ts between '$date_start 00:00:00' and '$date_end 23:59:00' group by 1 order by 1 asc";
$conn = $db_class->reporting_db_connection();
$query1 = pg_query($conn, $query);

$totals_results = pg_fetch_all($query1);

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Report by Date</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->

</head>
<body>
<!--MENÚ SUPERIOR-->
<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #fdd400">
    <a class="navbar-brand">KingMonetize SSP</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Dashboard</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Reports</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="by_date.php">By Date</a>
                    <a class="dropdown-item" href="by_tag.php">By Tag</a>
                    <a class="dropdown-item" href="by_subid.php">By SubID</a>
                    <a class="dropdown-item" href="by_country.php">By Country</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="filter_report.php">Fallbacks</a>
                    <a class="dropdown-item" href="postbacks.php">Postbacks</a>
                </div>
            </li>
        </ul>
        </li><a class="nav-link" href="skype:live:publishers_58?chat">Support</a> <a class="btn btn-light" style="margin-left: 10px" href="logout.php" role="button">Logout</a>
    </div>
</nav>

<!--ESPACIO ANTES DEL MAIN CONTENT-->
<div class="row my-1">
    <div class="col text-center">
        <h4>Postbacks Report</h4>
    </div>
</div>
<div class="dropdown-divider" style="margin-top: 8px"></div>

<!--ESPACIO ANTES DEL MAIN CONTENT-->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form name="DateFilter" method="post">
                <div class="row" style="margin-left: 320px">
                    <div class="col-md-auto">
                        <input type="date" class="form-control" name="date_start" required/>
                    </div>
                    <div class="col-md-auto">
                        <input type="date" class="form-control" name="date_end" required/>
                    </div>
                    <div class="col-md-auto">
                        <input type="submit" name="submit" value="Report" class="btn" style="background: gold"></input>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container" style="margin-top: 10px">

                <h2 class="text-center"><span class="badge badge-light">Actual Date Range: <?php echo $date_start;?> to <?php echo $date_end;?></span></h2>
                <!--TABLA-->
                <table class="table table-responsive table-bordered" style="margin-top: 10px">
                    <thead>
                    <tr>
                        <th scope="col" style="width: 16%">Date</th>
                        <th scope="col" style="width: 11%">Postbacks</th>
                        <th scope="col">Average Postback Payout</th>
                        <th scope="col">Revenue</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($totals_results as $row) {
                        echo "<tr>
                        <td>".str_replace('00:00:00','',$row['date'])."</td>
                        <td>".number_format($row['postbacks'],0,',','.')."</td>
                        <td>".number_format($row['cpm'],2,',','.')."</td>
                        <td>$".number_format($row['revenue'],2,',','.')."</td>
                    </tr>";
                    }
                    ?>
                    </tbody>
                </table>
                <table class="table table-responsive table-bordered" style="margin-top: 1px">
                    <thead>
                    <tr>
                        <th scope="col">---</th>
                        <th scope="col" style="width: 16%">Date</th>
                        <th scope="col" style="width: 11%">Postbacks</th>
                        <th scope="col">Average Postback Payout</th>
                        <th scope="col">Revenue</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        echo "<tr>
                        <td>Totals</td>
                        <td>".number_format($total_filtered_impressions,0,',','.')."</td>
                        <td>".number_format($total_under_validation_impressions,0,',','.')."</td>
                        <td>".number_format($total_validated_impressions,0,',','.')."</td>
                        <td>$".number_format($total_revenue,2,',','.')."</td>
                    </tr>";
                    ?>
                    </tbody>
                </table>
            </div>

    <!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>




</body>
</html>