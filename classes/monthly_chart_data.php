<?php

require "db_management.php";

session_start();
/*** begin the session ***/
if(!isset($_SESSION['publisher_id']))
{
    header('Location:login.php');
}
//**
$publisher_id = $_SESSION['publisher_id'];

// Connect to MySQL
$db_class = new db_management();

date_default_timezone_set('UTC');
$hoy = date('Y-m-d'); //01-12-2001
$dia = date('d');
$mes = date('m');
$ano = date('Y');
$hora_actual = date('H'); //17

$query = "SELECT tag_id from tags where publisher_id = '$publisher_id'";
$conn = $db_class->operation_db_connection();
$tag_query = pg_query($conn, $query);
$tag_fetch = pg_fetch_all($tag_query);
$tag_fetch_json = json_encode($tag_fetch);
$tag_list = str_replace('tag_id', '', str_replace('[', '', str_replace(']', '', str_replace('"', '', str_replace('{', '', str_replace('}', '', str_replace(':', '', $tag_fetch_json)))))));

pg_close($conn);

$conn = $db_class->reporting_db_connection();
// Fetch the data
$query = "
  SELECT  EXTRACT(DAY FROM ts) as day, sum(pub_payout) as revenue
  FROM reporting where tag_id in ($tag_list) and ts between '$ano-$mes-01 00:00:00.000000' and '$hoy $hora_actual:00:00.000000'
  group by 1 order by 1 asc";
$query_exec = pg_query($conn, $query);

// All good?
if ( !$query_exec ) {
    // Nope
    $message  = 'Invalid query: ' . $query_exec->error . "n";
    $message .= 'Whole query: ' . $query;
    die( $message );
}

    $data[] = array();
    // Print out rows
    while ($row = pg_fetch_assoc($query_exec)) {
        $data[] = $row;
    }

    header( 'Content-Type: application/json' );
    echo json_encode($data);
