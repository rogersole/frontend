<?php


class db_management
{
    public function getactualmonthtotals($publisher_id)
    {

        date_default_timezone_set('UTC');
        $hoy = date('Y-m-d'); //01-12-2001
        $dia = date('d');
        $mes = date('m');
        $ano = date('Y');
        $hora_actual = date('H'); //17

        $query = "SELECT tag_id from tags where publisher_id = '$publisher_id'";
        $conn = $this->operation_db_connection();
        $tag_query = pg_query($conn, $query);
        $tag_fetch = pg_fetch_all($tag_query);
        $tag_fetch_json = json_encode($tag_fetch);
        $tag_list = str_replace('tag_id', '', str_replace('[', '', str_replace(']', '', str_replace('"', '', str_replace('{', '', str_replace('}', '', str_replace(':', '', $tag_fetch_json)))))));

        $query = "select sum(impressions) as impressions, sum(pub_payout) as revenue, sum(pub_payout)/(sum(impressions)-sum(fallbacks))*1000 as cpm, sum(filtered) as filtered, sum(fallbacks) as fallbacks
                from reporting where tag_id in ($tag_list) and ts between '$ano-$mes-01 00:00:00.000000' and '$hoy $hora_actual:00:00.000000'";
        $conn = $this->reporting_db_connection();
        $query1 = pg_query($conn, $query);

        while($row1 = pg_fetch_assoc($query1)){
            $impressions = $row1['impressions'];
            $revenue = $row1['revenue'];
            $cpm = $row1['cpm'];
            $filtered = $row1['filtered'];
            $fallbacks = $row1['fallbacks'];
        }

        //** REQUESTS */

        $query = "select sum(requests) as requests from reporting_requests where tag_id in ($tag_list) and ts between '$ano-$mes-01 00:00:00.000000' and '$hoy $hora_actual:00:00.000000'";

        $query2 = pg_query($conn, $query);

        while($row4 = pg_fetch_assoc($query2)){
            $requests = $row4['requests'];
        }

        //** CALCULO POSTBACKS */


        $query = "select count(postback_amount) as postbacks from reporting_postbacks where tag_id in ($tag_list) and ts between '$ano-$mes-01 00:00:00.000000' and '$hoy $hora_actual:59:59.000000'";

        $query3 = pg_query($conn, $query);

        while($row4 = pg_fetch_assoc($query3)){
            $postbacks = $row4['postbacks'];
        }

        pg_close($conn);

        $monthly_results = array(
            'requests' => $requests,
            'impressions' => $impressions,
            'revenue' => $revenue,
            'cpm' => $cpm,
            'postbacks' => $postbacks,
            'fallbacks' => $fallbacks
        );

        return $monthly_results;
    }

    public function reporting_db_connection(){
        $dbconn_reporting = pg_connect("host= db.p.reporting.kingmonetize.com port=5432 dbname=reporting user=postgres password=toor");
        if (!$dbconn_reporting) {
            $error = "Error de conexión con PostgreSQL";
            file_put_contents("/tmp/logs/error.log", $error, FILE_APPEND);
            header("HTTP/1.0 502 Internal Services Error");
        }
        return $dbconn_reporting;
    }

    public function operation_db_connection(){
        $dbconn_reporting = pg_connect("host= db.p.operations.kingmonetize.com port=5432 dbname=postgres user=postgres password=toor");
        if (!$dbconn_reporting) {
            $error = "Error de conexión con PostgreSQL";
            file_put_contents("/tmp/logs/error.log", $error, FILE_APPEND);
            header("HTTP/1.0 502 Internal Services Error");
        }
        return $dbconn_reporting;
    }


}